from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
import time
import excel_info_sql
import biz_mysql
list1 = excel_info_sql.getConditions()
def Inovel_test():
    driver = webdriver.Chrome(r'D:\Program Files\python3.63\chromedriver.exe')
    driver.get("https://novel.youshuge.com/")
    driver.maximize_window()
    driver.implicitly_wait(10)


    # 定位账号
    driver.find_element_by_name('user_name').clear()
    driver.find_element_by_name('user_name').send_keys('{0}'.format(""))

    # 定位密码
    time.sleep(0.5)
    driver.find_element_by_name('password').clear()
    driver.find_element_by_name('password').send_keys('{0}'.format(''))

    # 点击登录
    time.sleep(6)
    driver.find_element_by_xpath('//button[@class="btn btn-block btn-primary"]').click()

    #弹出框
    # time.sleep(2)
    for m in range(0,1):
        try:
            WebDriverWait(driver,15).until(
                EC.element_to_be_clickable((By.XPATH,'//div[3]/button'))
            )
            driver.find_element_by_xpath("//div[3]/button").click()
            time.sleep(2)
        except Exception:
            print("未处理完弹出框问题")

    # 点击小说推广
    try:
        time.sleep(1)
        WebDriverWait(driver, 15).until(
            EC.element_to_be_clickable((By.XPATH, '//section/ul/li[5]//span[1]'))
        )
        driver.find_element_by_xpath('//section/ul/li[5]//span[1]').click()
    except Exception:
        print("点击小说推广")

    # 点击小说中心
    try:
        time.sleep(1)
        WebDriverWait(driver, 15).until(
            EC.element_to_be_clickable((By.XPATH, '//li[5]//li[1]/a'))
        )
        driver.find_element_by_xpath('//li[5]//li[1]/a').click()
    except Exception:
        print("点击小说中心")

    for i in range(0, len(list1)):
        # 小说名字
        Novel_name = list1[i]["Novel_name"]
        # print(Novel_name)
        # 日期
        insert_time = list1[i]["date_time"]
        # print(insert_time)
        # 章节
        Chapter = list1[i]["Chapter"]
        # print(Chapter)
        # 推广名称
        Extension_name = list1[i]["Extension_name"]
        # print(Extension_name)
        # 平台名称
        Platform = list1[i]["Platform"]

        #搜索框
        try:
            time.sleep(1)
            WebDriverWait(driver, 15).until(
                EC.element_to_be_clickable((By.XPATH, '//div[1]/div/div/div[1]//div[5]//input'))
            )
            driver.find_element_by_xpath('//div[1]/div/div/div[1]//div[5]//input').clear()
            time.sleep(1)
            driver.find_element_by_xpath('//div[1]/div/div/div[1]//div[5]//input').send_keys(Novel_name)
        except Exception:
            print("搜索框")

        #搜素按钮
        try:
            # time.sleep(2)
            WebDriverWait(driver, 15).until(
                EC.element_to_be_clickable((By.XPATH, '//form//button'))
            )
            driver.find_element_by_xpath('//form//button').click()
        except Exception:
            print("搜素按钮")
        #图片文案
        try:
            time.sleep(1)
            WebDriverWait(driver, 15).until(
                EC.element_to_be_clickable((By.XPATH, '//td[10]/div[1]/button'))
            )
            driver.find_element_by_xpath('//td[10]/div[1]/button').click()
        except Exception:
            print("图片文案")

        #选择章节 从1开始
        try:
            time.sleep(1)
            # print(type(Chapter))
            if Chapter == 1:
                Chapter = Chapter
            else:
                Chapter = Chapter -1
            driver.find_element_by_xpath('//td[10]//li[{0}]/a'.format(Chapter)).click()

        except Exception:
            print("选择章节")
        # 获得打开的第一个窗口句柄
        window_1 = driver.current_window_handle
        # print(window_1)
        # 获得打开的所有的窗口句柄
        windows = driver.window_handles
        # print(windows)
        # 切换到最新的窗口
        for current_window in windows:
            if current_window != window_1:
                driver.switch_to.window(current_window)

        #生成原文链接

        try:
            WebDriverWait(driver, 25).until(
                EC.element_to_be_clickable((By.XPATH, '//button[@class="btn btn-primary create_link"]'))
            )
            driver.find_element_by_xpath('//button[@class="btn btn-primary create_link"]').click()
        except Exception:
            print("生成原文链接")


        #渠道名称
        try:
            time.sleep(1)
            WebDriverWait(driver, 15).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="channel_name"]'))
            )
            driver.find_element_by_xpath('//*[@id="channel_name"]').clear()
            time.sleep(1)
            driver.find_element_by_xpath('//*[@id="channel_name"]').send_keys(insert_time+"_"+Extension_name)
        except Exception:
            print("渠道名称")

        #关注章节
        try:
            WebDriverWait(driver, 15).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="attention"]'))
            )
            driver.find_element_by_xpath('//*[@id="attention"]').clear()
            driver.find_element_by_xpath('//*[@id="attention"]').send_keys('8')
        except Exception:
            print("关注章节")

        #生成链接
        try:
            WebDriverWait(driver, 15).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="sub"]'))
            )
            driver.find_element_by_xpath('//*[@id="sub"]').click()
        except Exception:
            print('生成链接')
        # url
        try:

            time.sleep(2)
            url = driver.execute_script("return jQuery('[id=oneself_link]').val();")
            print(url)
            # 数据入库
            biz_mysql.insert_novel_mysql(url=url, Public_platform=Platform, Novel_name=Novel_name, Chapter=Chapter,datetime=insert_time, Extension_name=insert_time+"_"+Extension_name)
        except Exception:
            print("获取url失败")

        # 关闭
        try:
            driver.find_element_by_xpath('//span[text()="×"]').click()
        except Exception:
            print("关闭")
        driver.close()

        driver.switch_to.window(window_1)
    time.sleep(2)
    driver.quit()

if __name__ == "__main__":
    Inovel_test()