from openpyxl import load_workbook
def getConditions():
    #定义conditions为list列表
    conditions = []
    try:
        #打开Excel表格
        wb = load_workbook('有书阁.xlsx')
        #获取当前正在显示的sheet
        sheet = wb.active
        #定义一个数组,存储对应的参数
        array = ['Platform','Novel_name','Chapter','date_time','Extension_name']
        #定义行循环,循环第二行到 sheet.max+row 行
        for i in range(2,sheet.max_row + 1):
            #定义condition为字典数据类型
            condition = {}
            #使用enumerate定义列循环
            for j,arr in enumerate(array):
                #condition存入sheet.cell获取的单元格内容
                condition[arr] = sheet.cell(i,j+1).value
            #使用append将condition添加到新的对象
            conditions.append(condition)
        # print(conditions)
    except FileNotFoundError:
        print("有书阁.xlsx文件不存在")
    return conditions
# getConditions()
